import React from 'react'
import { View, Text,TextInput,StyleSheet,ScrollView,TouchableOpacity,Image } from 'react-native'

export default function Login() {
    return (
        <View style={styles.container}>
            <Image source={require('./src/asset/Logo.png')}/>
            <Text style={{fontSize:40,fontWeight: 'bold'}}>Login</Text>
            <View style={styles.form}>
                <View style={{backgroundColor:'skyblue',flex:1}}>
                    <Text>Name/User</Text>
                    <TextInput style={{height:50,borderRadius:4,borderColor:'black',borderWidth:1}}/>
                </View>
            </View>
            <View style={styles.password}>
                <View style={{backgroundColor:'skyblue',flex:1}}>
                    <Text>Password</Text>
                    <TextInput style={{height:50,borderRadius:4,borderColor:'black',borderWidth:1}}/>
                </View>
            </View>
            <View style={styles.bottomIn}>
                <TouchableOpacity>
                    <View style={{height:40,backgroundColor:'#3498db',justifyContent: 'center',alignItems: 'center',borderRadius:10,padding:20}}>
                        <Text>Masuk</Text>
                    </View>
                </TouchableOpacity>
                <Text style={{fontSize:20,marginTop:20}}>Atau</Text>
                <TouchableOpacity>
                    <View style={{height:40,backgroundColor:'white',justifyContent: 'center',alignItems: 'center',borderRadius:10,padding:20,borderWidth:2,marginTop:20}}>
                        <Text>Masuk</Text>
                    </View>
                </TouchableOpacity>
            </View>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        padding:20
    },
    form: {
        padding:20
    },
    password:{
        padding:20,
        marginTop:60
    },
    bottomIn:{
        marginTop:120,
        padding:20,
        backgroundColor:'white',
        justifyContent: 'center',
        alignItems: 'center'
    }
})
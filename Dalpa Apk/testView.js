import React from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  ScrollView,
  TouchableOpacity,
  Image,
  ImageBackground,
} from "react-native";

const TestView = () => {
  return (
    <ImageBackground
      source={require("./src/asset/latar.png")}
      style={styles.container}
    >
      <View
        style={{ flex: 1, borderColor: "blue", borderWidth: 2, padding: 20 }}
      >
        <View style={{ flex: 1, borderColor: "black", borderWidth: 2 }}></View>
      </View>
    </ImageBackground>
  );
};

const styles = StyleSheet.create({
  container: { width: "100%", height: "100%", flex: 1, marginTop: 40 },
});

export default TestView;

import React from 'react'
import { View, Text } from 'react-native'


import { NavigationContainer } from '@react-navigation/native'
import { createStackNavigator } from '@react-navigation/stack'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'



import Login from './pages/Login'
import Home from './pages/Home'
import About from './pages/About'
import Product from './pages/Product'
import Detail from './pages/Detail'

const Stack = createStackNavigator()
const Tab = createBottomTabNavigator()

export default function Router() {
    return (
        <NavigationContainer>
            <Stack.Navigator >
                <Stack.Screen name="Login" component={Login} options={{headerShown:false}} />
                <Stack.Screen name="Home" component={Home} options={{headerShown:false}} />
                <Stack.Screen name="About" component={About} options={{headerShown:false}} />
                <Stack.Screen name="Product" component={Product} options={{headerShown:false}} />
                <Stack.Screen name="Detail" component={Detail} options={{headerShown:false}} />
            </Stack.Navigator>
        </NavigationContainer>
    )}

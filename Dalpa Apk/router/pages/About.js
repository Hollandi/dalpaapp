import React,{useEffect,useState} from 'react'
import { View, Text,ImageBackground,StyleSheet,Button,TouchableOpacity,TextInput,Image} from 'react-native'

export default function About({navigation}) {


    return (
        <View style={styles.container}>
            <ImageBackground source={require('./src/asset/ABOUT.png')} style={styles.imgbaground}>
            </ImageBackground>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:25,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'black'
    },
    imgbaground:{
        height:'100%',
        width:'100%',
        justifyContent: "center",
        resizeMode:'cover'
}})

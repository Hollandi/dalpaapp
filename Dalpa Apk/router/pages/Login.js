import React,{useEffect,useState} from 'react'
import axios from 'axios'
import { View, Text,ImageBackground,StyleSheet,Button,TouchableOpacity,TextInput} from 'react-native'

export default function Login({navigation}) {
    const [user,setUsername] = useState('')
    const [password,setPassword] = useState('')
    
    const GetData = (user, password)=>{
        const data = {value: user,title:password}

        axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news',data)
        .then(response=>{
        if(data.value.length < 6){
            throw new Error('')
        }
        if(data.title.length < 6){
            throw new Error('')
        }
        navigation.navigate("Home")
        console.log('res',data)})
        .catch(()=>{console.log('gagal');alert("gagal Login silahkan masukkan dummy Username atau password dengan benar")})
        console.log(data)

    }

    return (
        <View style={styles.container}>
            <ImageBackground source={require('./src/asset/LATAR.png')} style={styles.imgbaground}>
                <Text style={{alignSelf:'center',color:'white',fontSize:42,fontWeight:'bold'}}>Login</Text>
                <View style={{justifyContent: 'center',alignItems: 'center',padding:20}}>
                    <Text style={{alignSelf:'flex-start',color:'white',fontSize:20,fontWeight:'bold'}}>Username</Text>
                        <View style={styles.inputer}>
                            <TextInput placeholder="Masukkan Username" onChangeText={(item)=>{setUsername(item)}}/>
                        </View>
                    <Text style={{alignSelf:'flex-start',color:'white',fontSize:20,fontWeight:'bold'}}>Password</Text>
                        <View style={styles.inputer}>
                            <TextInput placeholder="Password" secureTextEntry={true} onChangeText={(item)=>{setPassword(item)}} />
                        </View>                                    
                    <TouchableOpacity onPress={()=>{GetData(user,password)}}>
                        <View style={styles.loginBottum}>
                            <Text style={{fontSize:20,fontWeight:'bold',color:'#ecf0f1'}} > Login </Text>
                        </View>
                    </TouchableOpacity>
                    <TouchableOpacity style={{marginTop:20}}  onPress={()=>{alert("saat ini aplikasi masih dalam tahap beta silahkan masukkan dummy User untuk mengakses aplikasi")}}>
                        <View style={styles.signInBottum}>
                            <Text style={{fontSize:20,fontWeight:'bold',color:'#f1c40f'}}> Sign in </Text>
                        </View>
                    </TouchableOpacity>                    

                </View>
            </ImageBackground>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:25,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'black'
    },
    imgbaground:{
        height:'100%',
        width:'100%',
        justifyContent: "center",
        resizeMode:'cover'
    },
    text: {
    color: "white",
    fontSize: 42,
    fontWeight: "bold",
    textAlign: "center",
    backgroundColor: "#000000a0",
    
  },loginBottum:{height:50,
    width:200
    ,borderWidth:1,
    borderRadius:10,
    backgroundColor:'#f1c40f',
    justifyContent: 'center',
    alignItems: 'center'},

    signInBottum:{height:50,
    width:200,
    borderWidth:1,
    borderRadius:10,
    backgroundColor:'#ecf0f1',
    justifyContent: 'center',
    alignItems: 'center'},

    inputer:{
    width:300,
    height:50,
    borderWidth:1,
    borderRadius:10,
    backgroundColor:'#ecf0f1',
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom:50
    }
})
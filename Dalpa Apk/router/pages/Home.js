import React,{useEffect,useState} from 'react'
import { View, Text,ImageBackground,StyleSheet,Button,TouchableOpacity,TextInput,Image} from 'react-native'

export default function Home({navigation}) {


    return (
        <View style={styles.container}>
            <ImageBackground source={require('./src/asset/LATAR2.png')} style={styles.imgbaground}>
                <View style={styles.body}>

                    <TouchableOpacity onPress={()=>{navigation.navigate('Product')}}>
                    <View style={styles.content}>
                        <Image source={require('./src/asset/LambangP.png')} style={{height:130,width:170,}}/>
                        <Text style={{color:'white',fontWeight:'bold',fontSize:10}} >Kumpulan Kalam-Kalam</Text>
                    </View>
                    </TouchableOpacity>

                    <TouchableOpacity onPress={()=>{navigation.navigate('About')}} style={{marginTop:70}}>
                    <View style={styles.content2}>
                        <Image source={require('./src/asset/PROFILE.png')} style={{height:150,width:150}}/>
                        <Text style={{color:'white',fontWeight:'bold',fontSize:20}} >About Me</Text>
                    </View>
                    </TouchableOpacity>
                    
                </View>
            </ImageBackground>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:25,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'black'
    },
    imgbaground:{
        height:'100%',
        width:'100%',
        justifyContent: "center",
        resizeMode:'cover'
    },
    body:{
        flex: 1,
        justifyContent:'center',
        alignItems: 'center',
        padding:40
    },
    content:{height:150,
    width:300,
    backgroundColor:'#95a5a6',
    borderRadius:15,
    flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'},
    content2:{height:150,
    width:300,
    backgroundColor:'#34495e',
    borderRadius:15,
    flexDirection:'row',
    justifyContent: 'space-evenly',
    alignItems: 'center'}
})

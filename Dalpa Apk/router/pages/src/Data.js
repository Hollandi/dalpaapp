const Data = [
    {id:'1',image: require('./DataKalam/kalam1.jpg')},
    {id:'2',image: require('./DataKalam/kalam2.jpg')},
    {id:'3',image: require('./DataKalam/kalam3.jpg')},
    {id:'4',image: require('./DataKalam/kalam4.jpg')},
    {id:'5',image: require('./DataKalam/kalam5.jpg')},
    {id:'6',image: require('./DataKalam/kalam6.jpg')},
    {id:'7',image: require('./DataKalam/kalam7.jpg')},
    {id:'8',image: require('./DataKalam/kalam8.jpg')},
    {id:'9',image: require('./DataKalam/kalam9.jpg')},
    {id:'10',image: require('./DataKalam/kalam10.jpg')},
    {id:'11',image: require('./DataKalam/kalam11.jpg')},
    {id:'12',image: require('./DataKalam/kalam12.jpg')},
    {id:'13',image: require('./DataKalam/kalam13.jpg')},
    {id:'14',image: require('./DataKalam/kalam14.jpg')},
    {id:'15',image: require('./DataKalam/kalam15.jpg')},
    {id:'16',image: require('./DataKalam/kalam16.jpg')},
    {id:'17',image: require('./DataKalam/kalam17.jpg')},
    {id:'18',image: require('./DataKalam/kalam18.jpg')},
    {id:'19',image: require('./DataKalam/kalam19.jpg')},
    {id:'20',image: require('./DataKalam/kalam20.jpg')},
]

export { Data }
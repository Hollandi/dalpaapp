import React,{useEffect,useState} from 'react'
import { View, Text,ImageBackground,StyleSheet,Button,TouchableOpacity,TextInput,Image,FlatList} from 'react-native'
import { set } from 'react-native-reanimated'

import {Data} from './src/Data'

export default function Product({navigation}) {
    const [id,setId] = useState('')

    const nav = ()=>navigation.navigate("Detail")


    return (
        <View style={styles.container}>
            <ImageBackground source={require('./src/asset/LATAR2.png')} style={styles.imgbaground}>
                <Text style={{alignSelf:'flex-start',color:'white',fontSize:20,fontWeight:'bold',marginBottom:25,marginTop:25}}>Click gambar untuk ke halaman Bayan</Text>
                <FlatList data={Data}
                keyExtractor={(item) =>item.id}
                renderItem={({item})=>{
                    return(
                        <View style={{flex: 1,justifyContent:'center',alignItems: 'center'}}>
                            <TouchableOpacity onPress={()=>{
                                
                                setId(item.id);
                                console.log(`user menekan gambar dengan Id : ${item.id}`);
                                navigation.navigate("Detail")
                                
                            }}>
                                <Image source={item.image} style={{height:360,width:300,borderRadius:10,marginBottom:30}}/>
                                
                            </TouchableOpacity>
                            
                        </View>
                    )
                }}/>
            </ImageBackground>
        </View>
    )
}


const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop:25,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor:'black'
    },
    imgbaground:{
        height:'100%',
        width:'100%',
        justifyContent: "center",
        resizeMode:'cover'
}})

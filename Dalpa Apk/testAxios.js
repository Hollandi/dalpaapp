// Make a request
import axios from "./axios";
axios
  .get("https://achmadhilmy-sanbercode.my.id/api/v1/news")
  .then(function (response) {
    // handle success
    console.log(response);
  })
  .catch(function (error) {
    // handle error
    console.log(error);
  })
  .then(function () {
    // always executed
  });

// Send data to server
axios
  .post("https://achmadhilmy-sanbercode.my.id/api/v1/news", {
    title: "Indonesia Mengoding",
    value: "Bootcamp React Native",
  })
  .then(function (response) {
    console.log(response);
  })
  .catch(function (error) {
    console.log(error);
  });

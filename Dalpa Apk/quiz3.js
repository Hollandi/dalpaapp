import React,{useEffect,useState} from 'react'
import {Text,View} from 'react-native'

const Quiz = ()=>{
    const [name,setName] = useState('John Doe')
    useEffect(()=>{
        setTimeout(()=>{
            setName('Zaky Muhammad Fajar')
        },2000)
        return ()=>{
            setName('Achmad Hilmy')
        }
    },[])
    return (
        <View style={{alignItems: 'center',justifyContent: 'center'}}> 
            <Text>{name}</Text>
        </View>
    )
}

export default Quiz;
import React from 'react';
import {View, Text,TextInput,StyleSheet,ScrollView,TouchableOpacity,Image} from 'react-native'


export default function About(){
    return (
        <> 
        <View style={styles.container}>
            <View style={styles.header}>
                <Text style={{fontSize:20,fontWeight: 'bold'}}>Tentang Saya</Text>
                <Image source={require('./src/asset/PROFILE.png')}/>
                <Text style={{fontSize:18,fontWeight: 'bold',color:'#2980b9',marginTop:20}}> Muhammad Hollandi Sumari</Text>
                <Text style={{fontSize:18,fontWeight: 'bold',color:'black',marginTop:20}}> belajar di Sanbercode</Text>
            </View>
            <View style={styles.content}>
                <View style={{backgroundColor:'#7f8c8d',height:100,margin:20}}>
                <Text style={{fontSize:18,color:'black',marginLeft:20}}>Portofolio</Text>
                <View style={{backgroundColor:'black',height:2,width:300}}></View>
                <View style={styles.imageBind}>
                    <Image source={require('./src/asset/Vector.png')} style={{width:40,height:40}}/>
                    <Text style={{marginLeft:10}}>@Muhammad</Text>
                    <Image source={require('./src/asset/logo-github.png')} style={{width:40,height:40,marginLeft:20}}/>
                    <Text style={{marginLeft:10}}>@Anwari</Text>
                </View>
                </View>
            </View>
            <View style={styles.content}>
                <View style={{backgroundColor:'#7f8c8d',margin:20,height:150}}>
                <Text style={{fontSize:18,color:'black',marginLeft:20}}>Contact</Text>
                <View style={{backgroundColor:'black',height:2,width:300}}></View>
                <View style={styles.imageBind2}>
                    <Image source={require('./src/asset/logo-whatsapp.png')} style={{width:20,height:20}}/>
                    <Text style={{marginLeft:10}}>085246573447</Text>
                    <Image source={require('./src/asset/in.png')} style={{width:20,height:20,marginLeft:20}}/>
                    <Text style={{marginLeft:10}}>@AnwariLandi</Text>

                </View>
                </View>
            </View>
        </View>
        </>
    )
}


const styles = StyleSheet.create({
    container: {
        flex:0.7,
        backgroundColor:"white",
        marginTop:20,
        paddingTop:40},
        header:{
            padding:50,
            marginTop:100,
            height:50,
            justifyContent: 'center',
            alignItems: 'center'
        },
        content:{
            marginTop:120,
            flex:1,
        },
        imageBind:{
            flex:1,
            flexDirection:'row',
            alignItems: 'center',
            justifyContent: 'flex-start',
            padding:20
        },
        imageBind2:{
            flex:1,
            flexDirection:'row',
            alignItems: 'center',
            justifyContent: 'center',
            padding:20
        }


    })
